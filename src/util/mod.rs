use jni::JNIEnv;
use jni::sys::{jsize, jint, jobject, jvalue, jdouble};
use jni::signature::JavaType;
use jni::objects::JValue;
use jni::errors::Result;

#[derive(Clone)]
pub struct Client<'a> {
    env: *mut JNIEnv<'a>
}

impl<'a> Client<'a> {
    pub fn new(env: *mut JNIEnv<'a>) -> Self {
        Self {
            env
        }
    }
}

impl <'a> Client<'a> {
    pub fn get_mc(&mut self) -> Result<jobject> {
        let env = unsafe { *self.env };
        let mc_class = env.find_class("ave")?;
        let get_minecraft = env.get_static_method_id(mc_class, "A", "()Lave;")?;
        let mc = env.call_static_method_unchecked(mc_class, get_minecraft, JavaType::Object("".to_string()), &[])?;
        Ok(mc.l()?.into_inner())
    }

    pub fn get_player(mut self, mc: jobject) -> Result<Player<'a>> {
        let env = unsafe { *self.env };
        let mc = self.get_mc()?;
        let player = env.get_field(mc, "h", "Lbew;")?;
        let obj = player.l()?.into_inner();
        Ok(Player::new(obj, self.env, self))
    }
}

pub struct Player<'a> {
    player: jobject,
    env: *mut JNIEnv<'a>,
    client: Client<'a>,
}

impl<'a> Player<'a> {
    pub fn new(player: jobject, env: *mut JNIEnv<'a>, client: Client<'a>) -> Self {
        Self {
            player,
            env,
            client
        }
    }
}

impl<'a> Player<'a> {
    pub fn get_x(&mut self) -> Result<jdouble> {
        let env = unsafe { *self.env };
        Ok(env.get_field(self.player.clone(), "s", "D")?.d()?)
    }
}
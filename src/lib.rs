mod util;
use util::*;

mod hook;
use hook::*;

mod opengl;
use opengl::*;

use std::thread;
use std::arch::asm;
use std::ffi::c_void;
use std::io::Write;

use winapi::um::libloaderapi::{GetModuleHandleA, GetProcAddress};

use jni::JavaVM;
use jni::sys::{jsize, jint};

macro_rules! ptr {
    ($v: expr) => {
        $v.as_ptr().cast()
    };
}

extern { 
    fn init_console();
    fn free_console();
}

type hJNI_GetCreatedJavaVMs = fn (vmBuf: *mut *mut JavaVM, bufLen: jsize, nVMs: *mut jsize) -> jint;

pub unsafe fn pattern_scan(start_addr: *mut c_void, bytes: &[u8], size: usize) -> *mut c_void {
    println!("Scanning from address: {:?}", start_addr);

    'a: for i in 0..=0x1900 {
        for n in 0..size {
            let byte = *(((start_addr as usize + (size * i) + n) as *const usize) as *mut u8);
            // println!("{:?} | {:?}", bytes[n], byte);
            if bytes[n] != byte {
                continue 'a
            }
        }
        println!("Found pattern");
        return (start_addr as usize + (size * i)) as *mut c_void;
    }

    0x0 as *mut c_void
}

pub unsafe extern "C" fn swap_buffers() {
    // asm!("push rcx");
    // asm!("push rdx");

    // let opengl32dll = GetModuleHandleA(ptr!(b"opengl32.dll\0"));

    // asm!("pop rdx");
    // asm!("pop rcx");
    // let fn_glBegin = GetProcAddress(opengl32dll, ptr!(b"glBegin\0"));

    // let fn_glEnd = GetProcAddress(opengl32dll, ptr!(b"glEnd\0"));

    // asm!("mov rax, {}", in(reg) 1);
    // asm!("push rax");
    // asm!("push rbx");
    // asm!("mov rbx, {}", in(reg) fn_glBegin);
    // asm!("call rbx");
    
    // asm!("mov rax, {}", in(reg) fn_glEnd);
    // asm!("call rax");
    // asm!("pop rbx");
}

pub fn main_thread() {
    unsafe { init_console(); }
    unsafe {
        
        let opengl32dll = GetModuleHandleA(ptr!(b"opengl32.dll\0"));
        let addr = pattern_scan(
            opengl32dll as *mut c_void, 
            &[
                0x65, 0x48, 0x8B, 0x04, 0x25, 0x30, 0x00, 0x00, 0x00, 
                0x48, 0x8B, 0x80, 0x00, 0x0A, 0x00, 0x00,
                0x48, 0xFF, 0x25, 0x79, 0xEA, 0x0B, 0x00,
                0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC
            ], 
            29
        );
        println!("glBegin?: {:?}", addr);

        let jvmdll = GetModuleHandleA(ptr!(b"jvm.dll\0"));
        println!("jvm.dll: {:?}", jvmdll);

        let fn_get_created_java_vms = GetProcAddress(jvmdll, ptr!(b"JNI_GetCreatedJavaVMs\0"));
        let o_fn_get_created_java_vms: hJNI_GetCreatedJavaVMs = std::mem::transmute(fn_get_created_java_vms);
        println!("JNI_GetCreatedJavaVMs: {:?}", fn_get_created_java_vms);

        println!("Getting JVMs");
        let mut jvm: *mut JavaVM = std::ptr::null_mut();
        let mut jvm_count: jsize = 0;
        let res = o_fn_get_created_java_vms(&mut jvm as  _, 1, &mut jvm_count as _);
        println!("JVM Count: {}", jvm_count);

        println!("JNI_GetCreatedJavaVMs Response: {:?}", res);

        // let jvm = JavaVM::from_raw(jvm as _).expect("Failed to get JVM");
        // jvm.attach_current_thread_as_daemon().expect("Failed to attach to thread");

        // let mut env = match jvm.get_env() {
        //     Ok(env) => env,
        //     Err(e) => panic!("{:?}", e)
        // };
        // println!("Got JVMEnv");

        // Hooking
        println!("Getting wglSwapBuffers");
        let opengl32dll = GetModuleHandleA(ptr!(b"opengl32.dll\0"));
        println!("opengl32.dll: {:?}", opengl32dll);
        let fn_swapbuffers = GetProcAddress(opengl32dll, ptr!(b"wglSwapBuffers\0"));
        println!("wglSwapBuffers: {:?}", fn_swapbuffers);
        hook_fn(fn_swapbuffers as *mut c_void, swap_buffers as *mut c_void, 15);

        // let mut client = Client::new(&mut env as _);
        // let mc = client.get_mc();
        // println!("Minecraft Object: {:?}", mc);
        // let mc = match mc {
        //     Ok(mc) => mc,
        //     Err(e) => panic!("{:?}", e)
        // };
        
        // let mut exit = true;
        // while !exit {
        //     let mc_class = env.get_object_class(mc);
        //     println!("Minecraft Class: {:?}", mc);
        //     if mc_class.is_err() {
        //         std::thread::sleep(std::time::Duration::from_millis(1000));
        //         continue;
        //     }
        //     let player = env.get_field(mc, "h", "Lbew;");
        //     println!("Player Field: {:?}", player);
        //     match client.clone().get_player(mc) {
        //         Ok(mut player) => {
        //             println!("X: {:?}", player.get_x())
        //         },
        //         Err(e) => {
        //             println!("Error: {:?}", e)
        //         }
        //     }
        //     std::thread::sleep(std::time::Duration::from_millis(1000));
        // }
        
        // dealloc console
        // free_console();
    }
}

#[no_mangle]
pub extern "system" fn main() {
    thread::spawn(main_thread);
}
use std::ffi::c_void;

use winapi::um::memoryapi::{VirtualAlloc, VirtualProtect};
use winapi::um::winnt::{
  MEM_COMMIT, 
  MEM_RESERVE,
  PAGE_EXECUTE_READWRITE
};


pub unsafe fn hook_fn(fn_addr: *mut c_void, new_fn_addr: *mut c_void, len: usize) {

  // allocate space for trampoline
  let trampoline = VirtualAlloc(
    0 as *mut c_void,
    len + 26,
    MEM_COMMIT | MEM_RESERVE,
    PAGE_EXECUTE_READWRITE
  );

  println!("Trampoline {:?}", trampoline);

  let mut call_hook_bytes: [u8; 12] = [
    0x48, 0xb8, // movabs rax, hook_addr
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xff, 0xd0 // call rax
  ];
  let call_hook_bytes_ptr = call_hook_bytes.as_mut_ptr() as *mut c_void;
  std::ptr::copy_nonoverlapping(
    (&(new_fn_addr as usize) as *const usize) as *mut c_void,
    call_hook_bytes_ptr.offset(2),
    8
  );

  let mut return_bytes: [u8; 14] = [
    0xff, 0x25, 0x00, 0x00, 0x00, 0x00, // jmp [fn_addr+len]
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  ];
  let return_bytes_ptr = return_bytes.as_mut_ptr() as *mut c_void;
  std::ptr::copy_nonoverlapping(
    (&(fn_addr as usize + len) as *const usize) as *mut c_void,
    return_bytes_ptr.offset(6),
    8
  );

  std::ptr::copy_nonoverlapping(call_hook_bytes_ptr, trampoline, 12);
  std::ptr::copy_nonoverlapping(fn_addr, (trampoline as usize + 12) as *mut c_void, len);
  std::ptr::copy_nonoverlapping(return_bytes_ptr, (trampoline as usize + 12 + len) as *mut c_void, 14);

  // overwrite original function

  let mut original_protection: u32 = 0;
  let original_protection_ptr = &mut original_protection as *mut _;
  VirtualProtect(
    fn_addr,
    len,
    PAGE_EXECUTE_READWRITE,
    original_protection_ptr
  );

  std::ptr::write_bytes(fn_addr, 0x90, len); 

  let mut jmp_bytes: [u8; 14] = [
    0xFF, 0x25, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  ];

  let jmp_bytes_ptr = jmp_bytes.as_mut_ptr() as *mut c_void;

  // write destination address into jmp_bytes;
  std::ptr::copy_nonoverlapping(
    (&(trampoline as usize) as *const usize) as *mut c_void,
    jmp_bytes_ptr.offset(6),
    8,
  );

  println!("bytes to jump to trampoline: {:04x?}", jmp_bytes);

  std::ptr::copy_nonoverlapping(
    jmp_bytes_ptr,
    fn_addr,
    14,
  );
  
  VirtualProtect(
    fn_addr,
    len,
    original_protection as _,
    0 as *mut u32
  );
}
use std::ffi::c_void;

macro_rules! ptr {
    ($v: expr) => {
        format!("{}\0",$v).as_bytes().as_ptr().cast()
    };
}

use winapi::um::libloaderapi::{GetModuleHandleA, GetProcAddress};

pub unsafe fn gl_begin(mode: u32) -> c_void {
  let opengl32dll = GetModuleHandleA(ptr!("opengl32.dll"));
  let fn_glBegin = GetProcAddress(opengl32dll, ptr!("glBegin"));
  let glBegin: extern "stdcall" fn(mode: u32) -> c_void = std::mem::transmute(fn_glBegin);
  glBegin(mode)
}

pub unsafe fn gl_end() -> c_void {
  let opengl32dll = GetModuleHandleA(ptr!("opengl32.dll"));
  let fn_glEnd = GetProcAddress(opengl32dll, ptr!("glEnd"));
  let glEnd: extern "stdcall" fn() -> c_void = std::mem::transmute(fn_glEnd);
  glEnd()
}

pub unsafe fn gl_vertex_2f(x: f64, y: f64) -> c_void {
  let opengl32dll = GetModuleHandleA(ptr!("opengl32.dll"));
  let fn_glVertex2f = GetProcAddress(opengl32dll, ptr!("glVertex2f"));
  let glVertex2f: extern "stdcall" fn(x: f64, y: f64) -> c_void = std::mem::transmute(fn_glVertex2f);
  glVertex2f(x, y)
}
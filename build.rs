fn main() {
    cc::Build::new()
        .file("src/misc/console.c")
        .compile("phantom");
    println!("cargo:rerun-if-changed=src/misc/console.c");
}
# Phantom

Phantom is a Minecraft cheat made in Rust with WinAPI bindings and JNI.
When compiled the DLL does not have a DLLMain function. It is intended to be used with an injector like [infiltrate](https://git.envs.net/rookie/infiltrate).

Currently this is in a very basic state and will be further developed in future